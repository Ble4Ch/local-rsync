# https://hub.docker.com/_/alpine
ARG alpine_ver=3.13
FROM alpine:${alpine_ver}.5

ARG build_rev=4

LABEL org.opencontainers.image.source="\
    https://gitlab.com/Ble4Ch/local-rsync"

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            rsync \
            ca-certificates \
 && update-ca-certificates \
 && rm -rf /var/cache/apk/*
